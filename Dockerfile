ARG CENTOS_VERSION=8
FROM centos:$CENTOS_VERSION
RUN cat /etc/os-release

# Install Miniconda
# TODO: Should we or should we not use `conda update` ?
# TODO: Host this on dlweb ?
RUN curl -L https://repo.continuum.io/miniconda/Miniconda3-4.7.12.1-Linux-x86_64.sh -o /miniconda-installer.sh && \
    bash /miniconda-installer.sh -b -p /conda && \
    eval "$('/conda/bin/conda' 'shell.bash' 'hook' 2> /dev/null)" && \
    conda init && \
    conda config --set auto_update_conda False && \
    conda update -n base -c defaults conda && \
    conda install 'python==3.8.*' && \
    conda --version && \
    python --version

# Install some python packages using conda
# rather than pip.
RUN eval "$('/conda/bin/conda' 'shell.bash' 'hook' 2> /dev/null)" && \
    conda install 'psycopg2==2.8.*' && \
    conda install 'aiohttp==3.6.*'

# Install older 5.1.* version of pyyaml because
# odkewa and dlauth require it.
# TODO: update odkewa and dlauth ?
RUN eval "$('/conda/bin/conda' 'shell.bash' 'hook' 2> /dev/null)" && \
    conda install 'pyyaml==5.1.*'

# Install mod_wsgi 
# mod_wsgi didn't want to compile without redhat-rpm-config because it
# couldn't find `/usr/lib/rpm/redhat/redhat-hardened-cc1`
RUN yum install -y httpd-devel gcc redhat-rpm-config
RUN eval "$('/conda/bin/conda' 'shell.bash' 'hook' 2> /dev/null)" && \
    pip install 'mod_wsgi==4.6.*' && \
    pip install 'pycrypto==2.6.*' && \
    find /conda -name 'mod_wsgi*'

RUN eval "$('/conda/bin/conda' 'shell.bash' 'hook' 2> /dev/null)" && \
    conda install -c conda-forge 'bjoern==3.0.*'

# Install odkewa
COPY . /build
RUN eval "$('/conda/bin/conda' 'shell.bash' 'hook' 2> /dev/null)" && \
    cd /build && \
    pip install --extra-index-url 'https://dlweb.iri.columbia.edu/pip' . && \
    rm -rf /build
COPY ./keys /app/keys

# Set up httpd
COPY docker/odkewa/httpd.conf /etc/httpd/conf/httpd.conf
RUN rm -r /etc/httpd/conf.d /etc/httpd/conf.modules.d
COPY docker/odkewa/odkewa.wsgi /app/
# The following is bad security practice if running httpd as
# root, but we will run httpd as apache.
RUN chmod g+w /run/httpd

# Set up odkewa work directory
RUN install --directory --owner apache --group apache /work

# Set up path to odkewa configuration
ENV ODKEWA_CONFIG=/app/odkewa.yaml

COPY docker/entrypoint /usr/local/bin/entrypoint

# Run httpd as apache
USER apache
CMD ["/usr/local/bin/entrypoint"]

