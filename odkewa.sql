CREATE ROLE odkewa WITH LOGIN ENCRYPTED PASSWORD 'SNIP';
CREATE DATABASE odkewa WITH OWNER=odkewa ENCODING='UTF8';
\connect odkewa
CREATE EXTENSION postgis;
CREATE EXTENSION pgcrypto;

set role odkewa;


BEGIN TRANSACTION;

drop table xdatamedia;
drop table xdata;
drop table xmetamedia;
drop table xmeta;

drop table odkewa_import;


create table odkewa_import(
 running bool,
 ts timestamptz default now(),
 platform text,
 primary key (platform)
);
insert into odkewa_import (running, ts, platform) values (false,now(),'kobo');
insert into odkewa_import (running, ts, platform) values (false,now(),'ona');

create table xmeta(
 fid text not null, -- form id
 vid text not null, -- version id (commit)
 giturl text not null,
 path text not null,
 ts timestamptz default now(),
 xlsform bytea null,
 xform jsonb null,
 primary key (fid, vid)
);

create table xmetamedia(
 fid text not null,
 vid text not null,
 filename text not null,
 mimetype text not null, -- image/png, etc.
 language text null,
 content bytea not null,
 primary key (fid, vid, filename),
 foreign key (fid, vid) references xmeta (fid, vid)
);

create table xdata(
 sid text not null,
 fid text not null,
 vid text not null,
 device_id text null,
 device_ip text null,
 device_type text null,
 ts timestamptz default now(),
 xdata jsonb not null,
 primary key (sid),
 foreign key (fid, vid) references xmeta (fid, vid)
 -- hidden foreign key (xdata->...mediafields...) references xdatamedia(msid)
);

create table xdatamedia(
 msid text not null,
 filename text not null,
 mimetype text not null, -- image/png, etc.
 content bytea not null,
 ts timestamptz default now(),
 primary key (msid)
);

COMMIT TRANSACTION;

