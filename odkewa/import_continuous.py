#!/usr/bin/env python
import time, json, subprocess, sys
import psycopg2
from psycopg2 import errors as _errors
from psycopg2.extras import Json
import traceback
import datetime
from json import JSONDecodeError


class VidKeyException(Exception):
    pass


def odkewa_import(platform, config, dbpool):
   starttime=time.time()
   pconf = config[platform]
   while True:
      serialization_failure = False
      started = False
      try:
         with dbpool.take() as cm:
            with cm.resource as conn: # transaction
               r = cm.query1("update odkewa_import set running = true, ts = now() where platform=%s and running=false returning running;", (platform,))

         if r is not None:
            started = True
            logg(f"{platform}: import started")
            fs = load_form_list(platform, config)
            for form in fs:
               with dbpool.take() as cm:
                  try:
                     form = {k: v for k, v in form.items() if k in ('formid', 'last_submission_time', 'title', 'description', 'id_string', 'date_created', 'date_modified', 'uuid', 'num_of_submissions', 'version')}
                     load_form(cm, platform, config, form)
                  except _errors.SerializationFailure as e:
                     logg(f"{platform}: serialization failure")
                     serialization_failure = True
                     raise
                  except VidKeyException as e:
                     pass

            logg(f"{platform}: import completed, processed {len(fs)} forms")
         else:
            logg(f"{platform}: skipping import, it is running somewhere else")

      except Exception as e:
         logg(e)
         traceback.print_tb(e.__traceback__)
      finally:
         if started:
            with dbpool.take() as cm:
               with cm.resource: # transaction
                  cm.qexec("update odkewa_import set running = false where platform=%s;", (platform,))

      time.sleep(config['import_interval'] if not serialization_failure else 5)


################################

def to_datetime(s):
    if s is None:
        return None
    s = s.strip()
    if s[-1].upper() == 'Z':
        s = s[:-1] + "+00:00"
    r = datetime.datetime.fromisoformat(s)
    return r


def load_form_list(platform, config):
    pconf = config[platform]
    auth_token = pconf['token']
    rs_text = subprocess.check_output(["curl", "-X", "GET", pconf['forms'], "-H", f"Authorization: Token {auth_token}"], stderr=subprocess.DEVNULL, encoding="utf-8")
    try:
        rs = json.loads(rs_text)
    except JSONDecodeError as e:
        logg(f"{platform}: ERROR! couldn't parse form list <<<{rs_text!r}>>>")
        raise
    #return [r for r in rs if r['formid'] in (493141,409084)]  # test mode
    return rs

def load_form(cm, platform, config, form):
    pconf = config[platform]
    auth_token = pconf['token']
    formid = str(form['formid'])
    fid = form['uuid']
    url = pconf['forms']
    path = formid
    date_modified = to_datetime(form['date_modified'])
    num_of_submissions = form['num_of_submissions']

    # removing microseconds because submission time of submissions comes without them
    last_submission_time = to_datetime(form['last_submission_time'])
    if last_submission_time is not None:
        last_submission_time = last_submission_time.replace(microsecond=0)

    id_string = form['id_string']
    title = form['title']
    description = form['description']

    with cm.resource: # transaction
        rs = cm.query1("select count(*) as count, max(ts) as ts from xdata where fid = %s;", (fid,))
        count = rs['count']
        ts = rs['ts']

    incremental = pconf['incremental']

    # We have 2 cases in ona formids 241464 and 339004 where num_of_submissions is less
    # by 1 of the real number of submissions. Trying to work around this by utilizing
    # last_submission_time
    if num_of_submissions < count and (last_submission_time is None or last_submission_time > ts):
        incremental = False
    start = count if incremental else 0
    n = num_of_submissions - start

    if n > 0:
        load_xls = pconf['load_xls']
        if load_xls:
            xlsform = subprocess.check_output(["curl", "-X", "GET", pconf['head']+formid+pconf['xls_tail'], "-H", f"Authorization: Token {auth_token}"], stderr=subprocess.DEVNULL)
        else:
            xlsform = None

        load_json = pconf['load_json'] or 'version' not in form
        if load_json:
            xform_text = subprocess.check_output(["curl", "-X", "GET", pconf['head']+formid+pconf['json_tail'], "-H", f"Authorization: Token {auth_token}"], stderr=subprocess.DEVNULL, encoding="utf-8")
            xform = json.loads(xform_text)
        else:
            xform_text = None
            xform = None

        vid = form['version'] if 'version' in form else xform['version']

        if num_of_submissions < count:
            if last_submission_time is None or last_submission_time > ts:
                logg(f"{platform}: WARNING! {fid=!r}, {formid=!r} too many submissions in the database (){count} > {num_of_submissions}), switched off incremental")
            else:
                logg(f"{platform}: WARNING! {fid=!r}, {formid=!r} too many submissions in the database (){count} > {num_of_submissions}), however, we didn't switch off incremental because last_submission_time {last_submission_time.isoformat()!r} less or equal to max(_submission_time) in the database {ts.isoformat()!r}")

        if not pconf['load_json'] and 'version' not in form:
            logg(f"{platform}: WARNING! {fid=!r}, {formid=!r} couldn't find version, switched on load_json")

        logg(f"{platform}: loading form [incremental={'on' if incremental else 'off'}, load_xls={'on' if load_xls else 'off'}, load_json={'on' if load_json else 'off'}] {fid=!r}, {vid=!r}, {url=!r}, {path=!r}, date_modified={date_modified.isoformat()!r}, last_submission_time={last_submission_time.isoformat() if last_submission_time is not None else None!r}, {num_of_submissions=!r}, {id_string=!r}, {title=!r}, {description=!r};  loading {n} submissions")

        req = pconf['subs_url'] + formid + ('' if start == 0 else f"?start={start}")
        submissions = json.loads(subprocess.check_output(["curl", "-X", "GET", req, "-H", f"Authorization: Token {auth_token}"], stderr=subprocess.DEVNULL, encoding="utf-8"))

        with cm.resource: # transaction
            populate_xmeta_koboona(cm, fid, vid, url, path, xlsform, xform, date_modified)
            for xdata in submissions:
                sid = xdata['_uuid']

                vid_key = None
                for vk in pconf['vid_keys']:
                    if vk in xdata:
                        vid_key = vk
                        break

                if vid_key is None:
                    logg(f"{platform}: ERROR! {fid=!r}, {formid=!r} couldn't find vid_key in xdata {json.dumps(xdata)}, skipped form")
                    raise VidKeyException()

                vid = xdata[vid_key]
                submission_time = to_datetime(xdata['_submission_time'] + 'Z')
                xdata_text = json.dumps(xdata)
                populate_xmeta_koboona(cm, fid, vid, url, path, None, None, None)
                populate_xdata_koboona(cm, sid, fid, vid, None, None, None, xdata, submission_time)


def populate_xmeta_koboona(cm, fid, vid, url, path, xlsform, xform, ts):
   xlsform = psycopg2.Binary(xlsform)
   cm.qexec("""
            INSERT INTO xmeta (fid, vid, giturl, path, xlsform, xform, ts)
            SELECT %s, %s, %s, %s, %s, %s, %s
            WHERE NOT EXISTS (SELECT 1 FROM xmeta WHERE fid = %s AND vid = %s
                AND ts = %s AND xlsform is not null AND xform is not null)
            ON CONFLICT (fid, vid) DO UPDATE SET
                giturl = EXCLUDED.giturl,
                path = EXCLUDED.path,
                xlsform = COALESCE(EXCLUDED.xlsform, xmeta.xlsform),
                xform = COALESCE(EXCLUDED.xform, xmeta.xform),
                ts = COALESCE(EXCLUDED.ts, xmeta.ts)
            """,
            (fid, vid, url, path, xlsform, Json(xform), ts, fid, vid, ts))


def populate_xdata_koboona(cm, sid, fid, vid, device_id, device_ip, device_type, xdata, ts):
   cm.qexec("""
            INSERT INTO xdata (sid, fid, vid, device_id, device_ip, device_type, xdata, ts)
            SELECT %s, %s, %s, %s, %s, %s, %s, %s
            WHERE NOT EXISTS (SELECT 1 FROM xdata WHERE sid = %s and ts = %s)
            """,
            (sid, fid, vid, device_id, device_ip, device_type, Json(xdata), ts, sid, ts))


###########################


def logg(*args, **kwargs):
    """ Logger
    """
    print(*args, file=sys.stderr, **kwargs)
    # sys.stderr.flush()

if __name__ == '__main__':
   pass
