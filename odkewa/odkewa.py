#
# Copyright (c) 2018-2019 Innokentiy Kumshayev <kumshkesh@gmail.com>
# Copyright (c) 2018-2019 IRI, Columbia University
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
# this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
# 3. Neither the name of the copyright holder nor the names of its contributors
# may be used to endorse or promote products derived from this software without
# specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
import sys, os, pyaconf, io
from flask import Flask, render_template, request, g, Response, send_file
import json
import time
import odkewa.json2html as j2h
import odkewa.gen_sha1 as gs
import odkewa.populate_db as pd
import psycopg2
import psycopg2.extensions
from psycopg2.extras import Json
from queuepool.pool import Pool
from queuepool.psycopg2cm import ConnectionManagerExtended
import odkewa.import_continuous as ci
import threading
import odkewa as odkewa_module


def version_info():
    return dict(
        name=odkewa_module.name,
        version=odkewa_module.__version__,
        author=odkewa_module.__author__,
        email=odkewa_module.__email__,
        python_version=dict(
            major=sys.version_info.major,
            minor=sys.version_info.minor,
            micro=sys.version_info.micro,
            releaselevel=sys.version_info.releaselevel,
            serial=sys.version_info.serial,
        ),
    )


def service_info(config):
    return dict(
        name=odkewa_module.name,
        version=odkewa_module.__version__,
        author=odkewa_module.__author__,
        email=odkewa_module.__email__,
        dbpool=dict(
            name=config["dbpool"]["name"],
            host=config["dbpool"]["host"],
            port=config["dbpool"]["port"],
            user=config["dbpool"]["user"],
            capacity=config["dbpool"]["capacity"],
        ),
        mode=config["mode"],
        prefix=config["prefix"],
        support_email=config["support_email"],
    )


def process_info():
    return dict(
        name=odkewa_module.name,
        version=odkewa_module.__version__,
        mode=config["mode"],
        pid=os.getpid(),
        active_count=threading.active_count(),
        current_thread_name=threading.current_thread().name,
        ident=threading.get_ident(),
        main_thread_ident=threading.main_thread().ident,
        stack_size=threading.stack_size(),
        threads={
            x.ident: dict(
                name=x.name, is_alive=x.is_alive(), is_daemon=x.daemon
            )
            for x in threading.enumerate()
        },
    )

config = pyaconf.load(os.environ['ODKEWA_CONFIG'])
PFX = config['core_path']
APFX = config['admin_path']

dbpoolConf = config['dbpool']
dbpool = Pool(
   name = dbpoolConf['name'],
   capacity = dbpoolConf['capacity'],
   maxIdleTime = dbpoolConf['max_idle_time'],
   maxOpenTime = dbpoolConf['max_open_time'],
   maxUsageCount = dbpoolConf['max_usage_count'],
   closeOnException = dbpoolConf['close_on_exception'],
)
for i in range(dbpool.capacity):
   dbpool.put(ConnectionManagerExtended(
      name = dbpool.name + '-' + str(i),
      autocommit = False,
      isolation_level = psycopg2.extensions.ISOLATION_LEVEL_SERIALIZABLE,
      dbname = dbpool.name,
      host = dbpoolConf['host'],
      port = dbpoolConf['port'],
      user = dbpoolConf['user'],
      password = dbpoolConf['password'],
   ))
if dbpoolConf['recycle_interval'] is not None:
   dbpool.startRecycler(dbpoolConf['recycle_interval'])

########################################################
### ONA & KOBO IMPORTS
########################################################

kobo_import = threading.Thread(target=ci.odkewa_import, kwargs={'platform':'kobo', 'config':config, 'dbpool':dbpool}, name='kobo_import', daemon=True)
ona_import  = threading.Thread(target=ci.odkewa_import, kwargs={'platform':'ona','config':config, 'dbpool':dbpool}, name='ona_import', daemon=True)

ona_import.start()
time.sleep(1)
kobo_import.start()

########################################################
app = Flask(__name__, static_url_path=PFX)  # TODO: make static path different from dynamic paths


def jsonify(data):
   return Response( response=json.dumps(data), status=200, mimetype='application/json')


@app.route(PFX+'/form/<fid>/<vid>')
def xlsform(fid, vid):
   with dbpool.take() as cm:
      with cm.resource: # transaction
         form = j2h.JSON_xlsform(pd.getForm(cm, fid, vid), pfx=PFX).generateForm()
   return render_template('index.html', form=form)


@app.route(PFX+'/submit_form', methods=['POST'])
def submit_form():
   form_data = json.loads(request.data)
   form_data['device_ip'] = str(request.remote_addr)
   with dbpool.take() as cm:
      with cm.resource: # transaction
         sid = pd.populate_xdata(cm, form_data)
   return jsonify({"sid": sid})


@app.route(PFX+'/upload_media', methods=['POST'])
def submit_file():
   media_data = request.files['file']
   with dbpool.take() as cm:
      with cm.resource: # transaction
         msid = pd.populate_xdatamedia(cm, media_data)
   return jsonify({"msid": msid})


# ----------


@app.route(APFX+"/mforms")
def showformsmeta():
   with dbpool.take() as cm:
      with cm.resource: # transaction
         rs = cm.query("select fid,vid,giturl,path,ts::text from xmeta order by fid,vid;")
   return jsonify(rs)


@app.route(APFX+"/mform/<fid>/<vid>")
def showformmeta(fid,vid):
   with dbpool.take() as cm:
      with cm.resource: # transaction
         rs = cm.query("select fid,vid,giturl,path,ts::text,xform from xmeta where fid=%s and vid=%s;",(fid,vid))
   return jsonify(rs)


@app.route(APFX+"/submission/<sid>")
def showsubmission(sid):
   with dbpool.take() as cm:
      with cm.resource: # transaction
         rs = cm.query("select sid,fid,vid,device_id,device_ip,device_type,ts::text,xdata from xdata where sid=%s;",(sid,))
   return jsonify(rs)


@app.route(APFX+"/media/<msid>")
def showmedia(msid):
   with dbpool.take() as cm:
      with cm.resource: # transaction
         rs = cm.query("select msid,filename,mimetype,ts::text,content from xdatamedia where msid=%s;",(msid,))
   return send_file(io.BytesIO(rs[0]['content']), mimetype=rs[0]['mimetype'])


@app.route(APFX+"/mmedia/<msid>")
def showmediameta(msid):
   with dbpool.take() as cm:
      with cm.resource: # transaction
         rs = cm.query("select msid,filename,mimetype,ts::text from xdatamedia where msid=%s;",(msid,))
   return jsonify(rs)


# -------------------


@app.route(f"{APFX}/version-info")
def versionInfo():
    return jsonify(version_info())


@app.route(f"{APFX}/service-info")
def serviceInfo():
    return jsonify(service_info(config))


@app.route(f"{APFX}/process-info")
def processInfo():
    return jsonify(process_info())


# ------------------
def start():
   flaskConf = config['flask']

   app.run(
      host=flaskConf['host'],
      port=flaskConf['port'],
      debug = flaskConf['debug'],
      threaded=flaskConf['threaded'],
      processes=flaskConf['processes'],
      ssl_context=(flaskConf['ssl_cert'],flaskConf['ssl_key']),
   )

if __name__ == '__main__':
   start()
