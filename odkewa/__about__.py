""" Provides package name, author, and version info
    This inofrmation is used in setup.py and at runtime.
"""
name = 'odkewa'
author = 'Innokentiy Kumshayev'
email = 'kumshkesh@gmail.com'
version = '0.7.6'
