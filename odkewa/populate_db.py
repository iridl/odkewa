#
# Copyright (c) 2018-2019 Innokentiy Kumshayev <kumshkesh@gmail.com>
# Copyright (c) 2018-2019 IRI, Columbia University
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
# this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
# 3. Neither the name of the copyright holder nor the names of its contributors
# may be used to endorse or promote products derived from this software without
# specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
#
import sys, os, datetime
import psycopg2, hashlib, json
from psycopg2.extras import Json
import odkewa.xls2json as x2j
import odkewa.gen_sha1 as gs
from queuepool.pool import Pool
from queuepool.psycopg2cm import ConnectionManagerExtended
import pyaconf


def connect(dbpool_conf):
   cm = ConnectionManagerExtended(
      name = dbpool_conf['name'],
      autocommit = False,
      isolation_level = psycopg2.extensions.ISOLATION_LEVEL_SERIALIZABLE,
      dbname = dbpool_conf['name'],
      host = dbpool_conf['host'],
      port = dbpool_conf['port'],
      user = dbpool_conf['user'],
      password = dbpool_conf['password'],
   )
   cm.open()
   return cm


def populate_xmeta(cm, giturl, vid, path):
   fid = gs.gen_sha1(giturl+path)
   xform = x2j.generate_json_from(path)
   with open(path, 'rb') as b:
       x = b.read()
       xlsform = psycopg2.Binary(x)
   cm.qexec("""
            INSERT INTO xmeta (fid, vid, giturl, path, xlsform, xform)
            VALUES (%s, %s, %s, %s, %s, %s);
            """,
            (fid, vid, giturl, path, xlsform, Json(xform)))
   return fid, vid


def populate_xdata(cm, data):
   data['ts'] = datetime.datetime.utcnow()
   data['device_id'] = gs.gen_sha1(data['device_ip']+data['device_type'])
   sid = gs.gen_sha1(data)
   fid = data['form_id']
   vid = data['form_version']
   device_id = data['device_id']
   device_ip = data['device_ip']
   device_type = data['device_type']
   xdata = data['data']

   form = getForm(cm, fid, vid)['survey']

   cm.qexec("""
      INSERT INTO xdata (sid, fid, vid, device_id, device_ip, device_type, xdata)
      VALUES (%s, %s, %s, %s, %s, %s, %s);
      """,
      (sid, fid, vid, device_id, device_ip, device_type, Json(xdata)))
   return sid


def populate_xdatamedia(cm, data):
   content = data.stream.read()
   ts = datetime.datetime.utcnow()
   msid = gs.gen_sha1(content)
   cm.qexec("""
      INSERT INTO xdatamedia (msid, filename, mimetype, content)
      SELECT %s, %s, %s, %s
      WHERE NOT EXISTS (SELECT 1 FROM xdatamedia WHERE msid = %s)
      """,
      (msid, data.filename, data.mimetype, psycopg2.Binary(content), msid))
   return msid


def getForm(cm, fid, vid):
   r = cm.query1("""SELECT xform FROM xmeta WHERE fid = %s AND vid = %s""", (fid,vid))
   return r['xform']


if __name__ == '__main__':
   if len(sys.argv) == 2:
      populate_xmeta(sys.argv[1])
   elif len(sys.argv) == 1:
      print("NO ARGUMENT GIVEN")
   else:
      print("ONE ARGUMENT EXPECTED, MORE THAN ONE GIVEN")
