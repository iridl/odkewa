import hashlib

def gen_sha1(x):
   return hashlib.sha1(str(x).encode('UTF-8')).hexdigest()
