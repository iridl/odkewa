#!/bin/bash

# TODO: move this script out to miconf

# Tags the image odkewa:latest with the version number, and pushes the
# image to the iridl Docker Hub registry

# Does a few sanity checks, but isn't smart enough to notice if you
# built the latest image on a different branch and then switched to
# master before publishing. Don't do that!

registry=iridl
repository=odkewa

set -e

fail() {
  echo $1
  exit 1
}

cleanup() {
    # output to /dev/null to supress confusing message "Not logged in"
    # when script exits before completing the login step.
    docker logout > /dev/null
}

trap cleanup EXIT

git diff --exit-code || \
    fail "There are uncommitted changes."
# TODO: also fail if there are untracked files, or is that too much of
# a pain?

[[ $(git rev-parse --abbrev-ref HEAD) == master ]] || \
    fail "You're not on master."

# Get version number
version=$(python3 -c 'import odkewa; print(odkewa.__version__)')
[[ -n "$version" ]] || fail "Failed to get version number."

docker build -t $repository . || fail "Build failed."

docker login

# Tag the latest image with the version number, and push both tags
# ("latest" and the version number) to the registry.
for tag in $version latest; do
  docker tag $repository $registry/$repository:$tag
  docker push $registry/$repository:$tag
done

# docker logout happens in cleanup()
