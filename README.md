#ODK FORMS

This project is an application that allows us to generate data-collection forms from an excel spreadsheet.

#How to use. TODO: update , this appears to be outdated

To start you need an xls file that complies with XLSForm specifications.

First we use xls2json.py to convert our .xls (or .xlsx) file to JSON.

Next we convert it from JSON to HTML using json2html.py.

Now we are ready to run __init__.py to run our Flask application.

#Restrictions

* field names must comply with the following regular expression:  [a-zA-Z_][a-zA-Z0-9_]*


# Koboona Imports

* If `incremental` flag is on, you are not allowed to submit the data using odkewa UI. Otherwise some records will not be imported. It relies on count match.
* If import misses a form version update, then xform, xlsform, and ts of missed versions in xmeta will be set to null.

* `_submission_time` lacks time zone. Appears to be in UTC.

* A form should have at least 1 submission to get imported.
