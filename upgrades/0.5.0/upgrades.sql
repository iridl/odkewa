-- to be run as postgres
\connect odkewa

set role ikh;

begin transaction;

-- migrating 'xmeta.xform' to jsonb:
alter table xmeta rename column xform to t;
alter table xmeta add column xform jsonb null;
update xmeta set xform = t::jsonb;
alter table xmeta drop column t;

-- migrating 'xdata.xdata' to jsonb:
alter table xdata rename column xdata to t;
alter table xdata add column xdata jsonb not null default '{}';
update xdata set xdata = t::jsonb;
alter table xdata drop column t;
alter table xdata alter column xdata drop default;


commit transaction;
